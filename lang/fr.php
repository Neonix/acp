<?php
/*

* 
MINES
ASSOCIATION
BATIMENT
AGRICULTURES
COUTUMIER
FORMATION
PLAQUETTES DES HOTELS
BROUSSE & ILES
CALENDRIERS & AGENDA
PROVINCES
*/


	$page = $menu = array( 
		'ACCUEIL' 		=> 'ACCUEIL', 
		'AGRICULTURES' 	=> 'AGRICULTURES',
		'BATIMENT' 		=> 'BATIMENT',
		'MINES' 		=> 'MINES',
		'ASSOCIATION'	=> 'ASSOCIATION',
		'PLAQUETTES' 	=> 'PLAQUETTES DES HOTELS',
		'COUTUMIER' 	=> 'COUTUMIER',
		'FORMATION' 	=> 'FORMATION',

		'BROUSSE' 		=> 'BROUSSE & ILES',
		'CALENDRIERS' 	=> 'CALENDRIERS & AGENDA',
		'PROVINCES' 	=> 'PROVINCES',
	);


	$LANG = array(
	  'January' => 'January',
	  'February' => "February",
	  "March" => 'March \'quote\' or "quote"',
	  "April" => "April 'quote' or \"quote\"",
	);

