<?php
ini_set('upload_max_filesize', '500M');
ini_set('post_max_size', '550M');
ini_set('memory_limit', '1024M');
ini_set('max_input_time', 300);
ini_set('max_execution_time', 300);



// SOURCE + DESTINATION
$source = $_FILES["file-upload"]["tmp_name"];
$destination = $_FILES["file-upload"]["name"];
$error = "";

print_r($source . "<br>");
print_r($destination . "<br>");

// CHECK IF FILE ALREADY EXIST
if (file_exists($destination)) {
  $error = $destination." already exist.";
}

// ALLOWED FILE EXTENSIONS
if ($error == "") {
  $allowed = ["jpg", "jpeg", "png", "gif", "application/pdf", "pdf"];
  $ext = strtolower(pathinfo($_FILES["file-upload"]["name"], PATHINFO_EXTENSION));
  
  /*if (!in_array($ext, $allowed)) {
    $error = "$ext file type not allowed - " . $_FILES["file-upload"]["name"];
  }
  */
}


/* LEGIT IMAGE FILE CHECK
if ($error == "") {
  if (getimagesize($_FILES["file-upload"]["tmp_name"]) == false) {
    $error = $_FILES["file-upload"]["name"] . " is not a valid image file.";
  }
}
*/

/*
// FILE SIZE CHECK
if ($error == "") {
  // 1,000,000 = 1MB
  if ($_FILES["file-upload"]["size"] > 50000000) {
    $error = $_FILES["file-upload"]["name"] . " - file size too big!";
  }
}
*/
// ALL CHECKS OK - MOVE FILE
if ($error == "") {
  if (!move_uploaded_file($source, $destination)) {
    $error = "Error moving $source to $destination";
  }
}

// ERROR OCCURED OR OK?
if ($error == "") {
  echo $_FILES["file-upload"]["name"] . " upload OK";
} else {
  echo $error;
}
?>
