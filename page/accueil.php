<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width">
    <link href='fonts/fonts.css' rel='stylesheet' type='text/css'>
    <link href='css/main.css' rel='stylesheet' type='text/css'>


    <meta http-equiv="cache-control" content="max-age=0" />
    <meta http-equiv="cache-control" content="no-cache" />
    <meta http-equiv="expires" content="0" />
    <meta http-equiv="expires" content="Tue, 01 Jan 1980 1:00:00 GMT" />
    <meta http-equiv="pragma" content="no-cache" />

    <title>ACP - Noumea</title>
    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
  </head>


  <body id="<?php echo $_GET["page"] ?>">

<!--
  <a class="logo scroll scroll-1" href="#section-1">
    <span class="icon"> </span>
  </a>
-->

<nav class="main">
  <button class="hamburger"><span></span></button>
  <?php include('page/module/nav.php'); ?>
</nav>


<nav class="menu">
  <ul>
    <li class="menu-title">acp</li>
    <li><a class="scroll" href="?page=AGRICULTURES">Agricultures</a></li>
    <li><a class="scroll" href="?page=BATIMENT">Bâtiment</a></li>
    <li><a class="scroll" href="?page=MINES">Mines</a></li>
    <li><a class="scroll" href="?page=PLAQUETTES DES HOTELS">Plaquettes</a></li>
    <li><a class="scroll" href="#">Culturel</a></li>
    <li><a class="scroll" href="#">Formations</a></li>
    <li class="plus"><a class="scroll" href="#" >+</a></li>
  </ul>
</nav>

<nav class="subnavfooter">
    <ul>
      <li><a>Newsletter</a></li>
      <li><a>Facebook</a></li>
      <li><a>Boite à idée</a></li>
      <li><a>Courier des lecteurs</a></li>
      <li><a>Contacts</a></li>
      <li><a>Notre équipe</a></li>
    </ul>
</nav>

<nav class="dots">
  <a class="scroll scroll-1 tooltip tooltip-left active" href="#section-1" data-tooltip="home"></a>
  <a class="scroll scroll-2 tooltip tooltip-left" href="#section-2" data-tooltip="about"></a>
  <a class="scroll scroll-3 tooltip tooltip-left" href="#section-3" data-tooltip="work"></a>
  <a class="scroll scroll-4 tooltip tooltip-left" href="#section-4" data-tooltip="contact"></a>
</nav>


<section class="section-1" id="section-1">
  <h1 class="acp">Acp</h1>
  <p>La Grande agence à Petit prix</p>
</section>

<section class="section-2" id="section-2">
  <h2>About</h2>
  <p>I do stuff... and things.</p>
</section>

<section class="section-3" id="section-3">
  <h2>Work</h2>
  <p>This is what I do. I love what I do.</p>
</section>

<section class="section-4" id="section-4">
  <h2>Contact</h2>
  <p>Don't call me.</p>
</section>

<a class="codepen" href="" target="_blank">
  <i class="fab fa-codepen"></i>
</a>

<a class="geekstudios" href="#" target="_blank">
  <svg class="brand" id="logo" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 50 50" style="enable-background:new 0 0 50 50;" xml:space="preserve">
    <defs></defs>
    <mask id="image-mask">
      <circle id="outer" cx="50" cy="50" r="50" fill="white"></circle>
      <circle id="inner" cx="50" cy="50" r="25"></circle>
    </mask>
    <circle class="circle" cx="25" cy="25" r="25"></circle>
    <polygon class="mountains" points="34.1,25 27.7,13.9 21.3,25 19.2,28.7 16.5,24 13,30 9.5,36.1 14.9,36.1 16.5,36.1 23.5,36.1 27.7,36.1 40.5,36.1"></polygon>
  </svg>
</a>

<div class="brizli" data-tooltip="That's not what I would do."></div>
  
    <footer>
     <nav id="subnavfooter">
        <ul>
          <li>
            <a href="/">Newsletter</a>
          </li>
          <li>
            <a href="/">Facebook</a>
          </li>
          <li>
            <a href="/">Newsletter</a>
          </li>
          <li>
            <a href="/">Newsletter</a>
          </li>
        </ul>
      </nav>
    </footer>

    <script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>
    <script  src="js/index.js"></script>


  </body>
</html>

